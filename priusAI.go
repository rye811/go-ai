package main

import (
	"math/rand"
	"fmt"
)

//Assume I am whoAmI

func returnMove(sliceMain [][]int) []int {
	var(  a int = -1
		 		b int = -1)
	for i := 0; i < len(sliceMain); i++{ //check for pieces surrounded by enemies
		for j := 0; j < len(sliceMain[i]); j++{
			if sliceMain[i][j] == whoAmI{ //If it's me
				numberOfEnemies := 0
				if isInvalidMove(sliceMain, i-1, j){
					numberOfEnemies++
					fmt.Println("ENEMIES IS t1")
				}
				if isInvalidMove(sliceMain, i+1, j){
					numberOfEnemies++
					fmt.Println("ENEMIES IS t2")
				}
				if isInvalidMove(sliceMain, i, j-1){
					numberOfEnemies++
					fmt.Println("ENEMIES IS t3")
				}
				if isInvalidMove(sliceMain, i, j+1){
					numberOfEnemies++
					fmt.Println("ENEMIES IS t4")
				}
				if numberOfEnemies == 3{
					fmt.Println("ENEMIES IS 3")
					if !isInvalidMove(sliceMain, i-1, j){
						a = i-1
						b = j
						break
					}
					if !isInvalidMove(sliceMain, i+1, j){
						a = i+1
						b = j
						break
					}
					if !isInvalidMove(sliceMain, i, j-1){
						a = i
						b = j-1
						break
					}
					if !isInvalidMove(sliceMain, i, j+1){
						a = i
						b = j+1
					}
				}
			}
		}
	}
	for {
		if isInvalidMove(sliceMain, a-1, b) && isInvalidMove(sliceMain, a+1, b) && isInvalidMove(sliceMain, a, b-1) && isInvalidMove(sliceMain, a, b+1){ //if move is suicide
			a = -1 //get random values
			b = -1
		}
		if a == -1 {
			a = rand.Intn(19)
		}
		if b == -1 {
			b = rand.Intn(19)
		}
		fmt.Println("BOT moved:",b,a) //b is the row, a is the column
		if sliceMain[a][b] == 0 {
			return []int{a,b}
		}
	}
}

func isInvalidMove(sliceMain [][]int, i int, j int) bool{
	if i < len(sliceMain) && j < len(sliceMain) && i > -1 && j > -1 && sliceMain[i][j] == 0{
		return false
	}else{
		return true
	}
}

//func centerOfEnemy(sliceMain){

//}
